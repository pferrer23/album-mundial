import Controller from '@ember/controller';
import { get, set, computed } from '@ember/object';
import { A } from '@ember/array';

export default Controller.extend({
  filterValue: "all",
  legends: A([660, 661, 662, 663, 664, 665, 666, 667, 668, 669]),
  logos: A([20, 40, 60, 80, 100, 120, 140, 160, 180, 200, 220, 240, 260, 280, 300, 320, 340, 360, 380, 400, 420,440,460,480,500,520,540,560,580,600,620,640]),
  stadiums: A([8,9, 10,11,12,13,14,15,16,17,18,19]),
  firstPage: A([1,2,3,4,5,6,7]),
  pendingCount: computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count === 0;
    }).length;
  }),
  repeatedCount:  computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count > 1;
    }).length;
  }),
  logosCount: computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count > 0 && get(this, 'logos').includes(sticker.sticker);
    }).length;
  }),
  stadiumsCount: computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count > 0 && get(this, 'stadiums').includes(sticker.sticker);
    }).length;
  }),
  legendsCount: computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count > 0 && get(this, 'legends').includes(sticker.sticker);
    }).length;
  }),
  firstPageCount: computed('model', 'filterValue', 'searchString', function() {
    return get(this, 'model').filter(sticker => {
        return sticker.count > 0 && get(this, 'firstPage').includes(sticker.sticker);
    }).length;
  }),
  filteredStickers: computed('model', 'filterValue', 'searchString', function() {
    if (get(this, 'searchString')) {
      return get(this, 'model').filter(sticker => {
        if (get(this, 'filterValue') === "all") {
          return sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "pending") {
          return sticker.count === 0 && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "repited") {
          return sticker.count > 1 && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "firstPage") {
          return get(this, 'firstPage').includes(sticker.sticker) && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "stadiums") {
          return get(this, 'stadiums').includes(sticker.sticker) && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "logos") {
          return get(this, 'logos').includes(sticker.sticker) && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
        if (get(this, 'filterValue') === "legends") {
          return get(this, 'legends').includes(sticker.sticker) && sticker.sticker.toString().includes(get(this, 'searchString').toString());
        }
      });
    } else {
      return get(this, 'model').filter(sticker => {
        if (get(this, 'filterValue') === "all") {
          return true;
        }
        if (get(this, 'filterValue') === "pending") {
          return sticker.count === 0;
        }
        if (get(this, 'filterValue') === "repited") {
          return sticker.count > 1;
        }
        if (get(this, 'filterValue') === "firstPage") {
          return get(this, 'firstPage').includes(sticker.sticker);
        }
        if (get(this, 'filterValue') === "stadiums") {
          return get(this, 'stadiums').includes(sticker.sticker);
        }
        if (get(this, 'filterValue') === "logos") {
          return get(this, 'logos').includes(sticker.sticker);
        }
        if (get(this, 'filterValue') === "legends") {
          return get(this, 'legends').includes(sticker.sticker);
        }
      });
    }
  }),
  actions: {
    addSticker(sticker, stickerList) {
      let count = get(sticker, 'count');
      count ++;
      set(sticker, 'count', count);
      localStorage.setItem("stickers", JSON.stringify(stickerList));
    },
    updateCount(sticker) {
      set(this, "selectedSticker", sticker);
      set(this, "isShowingModal", true);
    },
    filter(filterValue) {
      set(this, 'filterValue', filterValue);
    },
    closeModal() {
      set(this, "isShowingModal", false);
    },
    deleteSticker(sticker, stickerList) {
      let count = get(sticker, 'count');
      count --;
      set(sticker, 'count', count);
      localStorage.setItem("stickers", JSON.stringify(stickerList));
    }
  }
});
